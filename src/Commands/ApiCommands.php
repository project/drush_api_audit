<?php

namespace Drupal\drush_api_audit\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Url;
use Drush\Commands\DrushCommands;

/**
 * A Drush command file for commands to audit API routes.
 */
class ApiCommands extends DrushCommands {

  /**
   * Drupal route provider service.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected RouteProviderInterface $routeProvider;

  /**
   * Constructs a new ApiCommands object.
   */
  public function __construct(RouteProviderInterface $routeProvider) {
    parent::__construct();

    $this->routeProvider = $routeProvider;
  }

  /**
   * View information about all API routes or one API route.
   *
   * @param array $options
   *   The options array.
   *
   * @command api:route
   * @aliases ar
   * @options name A route name.
   * @options path An internal path or URL.
   * @options format output format, default is yaml.
   * @usage drush api:route
   *
   * @return array
   *   All API routes.
   */
  public function route(
    array $options = [
      'name' => self::REQ,
      'path' => self::REQ,
      'format' => 'yaml',
    ],
  ) {
    $route = $items = NULL;
    $provider = $this->routeProvider;
    if ($path = $options['path']) {
      if (filter_var($path, FILTER_VALIDATE_URL)) {
        $path = parse_url($path, PHP_URL_PATH);
        // Strip base path.
        $path = '/' . substr_replace($path, '', 0, strlen(base_path()));
      }
      $name = Url::fromUserInput($path)->getRouteName();
      $route = $provider->getRouteByName($name);
    }
    elseif ($name = $options['name']) {
      $route = $provider->getRouteByName($name);
    }

    if ($route) {
      $format = $route->getRequirement('_format');
      if ($format && $format !== 'html') {
        $return = [
          'name' => $name,
          'path' => $route->getPath(),
          'defaults' => $route->getDefaults(),
          'requirements' => $route->getRequirements(),
          'options' => $route->getOptions(),
          'methods' => $route->getMethods(),
        ];
        unset($return['options']['compiler_class'], $return['options']['utf8']);
        return $return;
      }
    }
    else {
      $routes = $this->getAllApiRoutes();
      foreach ($routes as $route_name => $api_route) {
        $items[$route_name] = $api_route->getPath();
      }
    }

    return $items;
  }

  /**
   * Audit API route permission.
   *
   * @param array $options
   *   The command options.
   *
   * @command api:audit:permission
   * @aliases api-perm
   * @field-labels
   *   name: Route name
   *   path: Route path
   *   requirements: Route requirements
   * @default-fields name,path,requirements
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields|string
   *   The open access routes in a RowsOfFields object.
   */
  public function auditPermission(array $options = ['format' => 'table']) {
    $api_routes = $this->getAllApiRoutes();
    $rows = [];

    foreach ($api_routes as $route_name => $route) {
      $requirements = $route->getRequirements();
      $is_open_access = $this->isOpenAccessRoute($requirements);

      if ($is_open_access) {
        $rows[] = [
          'name' => $route_name,
          'path' => $route->getPath(),
          'requirements' => Yaml::encode($requirements),
        ];
      }
    }

    if (empty($rows)) {
      return 'No open access API route found.';
    }

    return new RowsOfFields($rows);
  }

  /**
   * Get all API related routes.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of routes keyed by route name.
   */
  private function getAllApiRoutes() {
    // An array of all API related routes.
    $api_routes = [];

    // A list of all routes.
    $routes = $this->routeProvider->getAllRoutes();
    foreach ($routes as $route_name => $route) {
      $format = $route->getRequirement('_format');

      if ($format && $format !== 'html') {
        $api_routes[$route_name] = $route;
      }
    }

    return $api_routes;
  }

  /**
   * Check whether a route is an open access route.
   *
   * @param string[] $requirements
   *   The route requirement array.
   *
   * @return bool
   *   Return true if it is a open access route.$this
   *   Otherwise, return false.
   */
  private function isOpenAccessRoute(array $requirements) {
    $open_access = FALSE;
    if ($access = $requirements['_access'] ?? FALSE) {
      // Search for _access: 'TRUE'.
      if (is_string($access) && strtolower($access) === 'true') {
        $open_access = TRUE;
      }
    }
    elseif ($permission = $requirements['_permission'] ?? FALSE) {
      // Search for 'access content' permission.
      // 'access content' is normally granted to anonymous user.
      if (strtolower($permission) === 'access content') {
        $open_access = TRUE;
      }
    }
    elseif ($role = $requirements['_role'] ?? FALSE) {
      // Search for _role: 'anonymous'.
      if (is_string($role) && strtolower($role) === 'anonymous') {
        $open_access = TRUE;
      }
    }
    elseif ($login = $requirements['_user_is_logged_in'] ?? FALSE) {
      // Search for _user_is_logged_in: 'FALSE'.
      if (is_string($login) && strtolower($login) === 'false') {
        $open_access = TRUE;
      }
    }

    return $open_access;
  }

}
