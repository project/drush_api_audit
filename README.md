# API Audit Drush Command

This module provides Drush commands for auditing Headless/Decoupled API routes.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/drush_api_audit).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/drush_api_audit).


## Table of contents

- Requirements
- Installation
- Configuration
- Commands


## Requirements

This module requires [Drush 11 or 12](https://www.drush.org)

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

This is Drush command module, which means no configuration required, as long as Drush can be run under your Drupal root folder.

## Commands

- List all API(endpoint) routes.
    
    drush api:route
      
      Options:
        
        --name=NAME                        
        --path=PATH                        
        --format[=FORMAT]  [default: yaml] 

      Global options:
        -v|vv|vvv, --verbose Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug 
        -y, --yes            Auto-accept the default for all user prompts. Equivalent to --no-interaction.                      
        -l, --uri=URI        A base URL for building links and selecting a multi-site. Defaults to https://default.             
                     To see all global options, run `drush topic` and pick the first choice.                            

      Aliases: ar

- Audit API(endpoint) route's permissions.

    drush api:audit:permission

      Options:

        --format[=FORMAT]  [default: table] 

      Global options:
        -v|vv|vvv, --verbose Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug 
        -y, --yes            Auto-accept the default for all user prompts. Equivalent to --no-interaction.                      
        -l, --uri=URI        A base URL for building links and selecting a multi-site. Defaults to https://default.             
                     To see all global options, run `drush topic` and pick the first choice.                            

      Topics:
        drush topic docs:output-formats-filters Output formatters and filters: control the command output 


      Aliases: api-perm

    
