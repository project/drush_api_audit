<?php

declare(strict_types=1);

namespace Drupal\Tests\drush_api_audit\Functional;

use Drupal\Tests\BrowserTestBase;
use Drush\TestTraits\DrushTestTrait;

/**
 * Test route related drush commands.
 *
 * @group drush_api_audit
 */
final class RouteCommandsTest extends BrowserTestBase {
  use DrushTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drush_api_audit',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests drush api:route with feedback.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function testListAllApiRoutes(): void {
    $this->drush('ar');
    $this->assertStringContainsString('user.login.http: /user/login', $this->getOutput());
  }

  /**
   * Tests drush api:audit:permission with feedback.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function testRoutePermissionAudit(): void {
    $this->drush('api-perm');
    $this->assertStringContainsString('user.pass.http', $this->getOutput());
  }

}
